$(function () {
  // value mapping
  var donationMapping = {
    "001" : "19",
    "002" : "23",
    "003" : "28",
  }

//variables for processing fee + donation amount
  var totalDonation;
  var processingFees;
  var donationNum;
  var donationFixed;
  var calculatedFee;
  var donationString;
  var donationOutput;
  var decimals;  

  function updateDonation() {
    localStorage.setItem('donation', donationCode);
    localStorage.setItem('amount', donationAmt);
    localStorage.setItem('total-donation', totalDonation);
    localStorage.setItem('donation-output', donationOutput);
  }

  function calcTotalDonation () {
    processingFees = 0.03;

    //use parse float to get decimal places (primarily for use on "other" donation value)
    donationNum = parseFloat(donationAmt);

    //normalize user input to have only 2 decimal places
    donationFixed = donationNum.toFixed(2);

    //get the fee to be charged
    calculatedFee = donationFixed * processingFees;

    //calculate total donation
    totalDonation = donationNum + donationFixed * processingFees;

    //consoleDump();

    //check if donation is number or not

    if (isNaN(totalDonation)){

      $("#total-donation").text(" ");

    }

    else {

    //strictly for viewing by the user, not for calculating  
    //convert donation # to a string so we can normalize it to 2 decimal places

      donationString = totalDonation.toString();

    //split the string into 2 arrays at decimal point  

      donationString = donationString.split(".");

    //target decimals array (array #2), then get only to the hundreths place & ignore all others after  

      decimals = donationString[1].slice(0, 2);

    //combine the whole number (array #1) with the new, normalized decimal number  



    //check if decimals only has one number (ex: 0.6)
      if (decimals.length == 1){

        //convert decimals to string
        decimals = decimals.toString();


        //add zero to the end of the decimals string
        donationOutput = donationString[0] + "." + decimals + 0; 

        console.log(donationOutput);

        //output the new string to user
        $("#total-donation").text("$" + donationOutput);

      }

     //if decimals has more than 1 number  

      else {

        //combine the whole number (array #1) with the new, normalized decimal number 
        donationOutput = donationString[0] + "." + decimals;

        console.log(donationOutput);

        
        //output the new string to user
        $("#total-donation").text("$" + donationOutput);


      }

      //outputDump();

    

      updateDonation();
    }
    
  }

  // set donation amount 
     var donationCode = localStorage.getItem('donation') || '';
     var donationAmt = localStorage.getItem('amount') || '';

     updateDonation();
     calcTotalDonation();

  if (donationCode == '004') {
    $("#amountOther").prop("checked", true);
    $(".donation-other").attr("disabled");
  } else if (donationCode == '003') {
    $("#amount28").prop("checked", true); 
  } else if (donationCode == '002') {
    $("#amount23").prop("checked", true);
  } else {
    $("#amount19").prop("checked", true);
  }

  // uncomment console dump to help with debugging
  function consoleDump() {
    console.log("Dontaion Code " + donationCode + ": donaton amount $" + donationAmt + ": fixed donation amount $"+ donationFixed +" total dono: $" + totalDonation + " fee :" + calculatedFee);
  }

  function outputDump() {
    console.log("decimals :" + decimals + " split donation array :" + donationString + " donation output :" + donationOutput);
  }


  // adjust callout for donation
  $("input[name$='selection']").click(function() {
    $("#total-donation").text(" ");
    donationCode = $(this).val();
    donationAmt = donationMapping[donationCode];

    if (donationCode == 004) {
      donationAmt = $('#giftAmount').val();
      updateDonation();
    
      $('#giftAmount').keyup(function(event) {
        donationAmt = event.target.value;
        $('#giftAmount').val(donationAmt);
       /* var result = parseInt(donationAmt);
        donationAmt = result.toFixed(2);*/
        calcTotalDonation();
        updateDonation();

      });
    
      $(".input-group [type=number]").on('input', function(){
        donationAmt = event.target.value;
        updateDonation();
        calcTotalDonation();
      });
    
    } else {
      updateDonation();
      calcTotalDonation();

    }
   // consoleDump();

  });

  // calculate total donation w/ processing fees [3% of total donation]



});