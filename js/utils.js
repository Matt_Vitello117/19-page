$(document).ready(function(){

  //check if donation choice exists, if yes select that amount
     var donationCode = localStorage.getItem('donation') || '';
     var donationAmt = localStorage.getItem('amount') || '';
     var totalDonation = localStorage.getItem('total-donation') || '';
     var donationOutput = localStorage.getItem('donation-output') || '';

    if ($(donationAmt.length > 0)){

      //get donation amt form localStorage + populate the other field

      if (donationCode == 004){
        $(".donation-other").removeClass("disabled");
        $("#giftAmount").removeAttr("readOnly");
        $('#giftAmount').val(donationAmt);
        $('#gift-4').addClass("donation-selected");
        $("#total-donation").text("$" + donationOutput);
      }

      if (donationCode == 001){
        $(".donation-choice").removeClass("donation-selected");
        $('#gift-1').addClass("donation-selected")
      }

      if (donationCode == 002){
        $(".donation-choice").removeClass("donation-selected");
        $('#gift-2').addClass("donation-selected")
      }

      if (donationCode == 003){
        $(".donation-choice").removeClass("donation-selected");
        $('#gift-3').addClass("donation-selected")
      }
      
      else if (donationCode == undefined) {
        $(".donation-choice").removeClass("donation-selected");
      }
    }

		$(".donation-choice").click(function(){
		$('.donation-choice').removeClass("donation-selected");
	    $(this).addClass("donation-selected");
	});


	$("input[type=radio][name=selection]").click(function(){		
      
      if($(this).val()=="004"){
				$("#giftAmount").removeAttr("readOnly");
				$(".donation-other").removeClass("disabled");			
				$("#giftAmount").focus();
			} else {
				$("#giftAmount").attr("readOnly", "readOnly");
        $("#giftAmount").val("");
				$(".donation-other").addClass("disabled");	
			}
			
		});


  //show/hide total donation + processing fees on user choice 

  $("input[name='acceptFees']").change(function(){

    $(".total-donation-container").toggle();

  });

	// privacy modal
  $('#privacyModal').on('show.bs.modal', function (e) {
      var button = $(e.relatedTarget);
      var modal = $(this);
      modal.find('.modal-body').load(button.data("remote"));
  });

  // charitable solicitation modal
  $('#csModal').on('show.bs.modal', function (e) {
      var button = $(e.relatedTarget);
      var modal = $(this);
      modal.find('.modal-body').load(button.data("remote"));
  });

// terms modal
  $('#termsModal').on('show.bs.modal', function (e) {
      var button = $(e.relatedTarget);
      var modal = $(this);
      modal.find('.modal-body').load(button.data("remote"));
  });


});